# samchristianinfo

Sam Christian's personal website

# Requirements

* Hugo - Static website builder

# To Run
```
$ git submodule update --init --recursive
$ git submodule update --recursive --remote
$ hugo
```
