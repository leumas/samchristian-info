---
url: /resume
layout: resume
---

# Sam Christian

Backend software engineer with experience in threat research and security.

**Python, Gitlab, AWS, Docker, Continuous Integration/Continuous Delivery**

**Angular, Flask, Rest, Elasticsearch, API, Terraform, Ansible, FastAPI, Serverless**

**Test Driven Development, Golang, Kubernetes**

## Salesforce | Lead Threat Intelligence Software Engineer | March 2022 - Present

* Lead developer of intelligence ecosystem system, providing security organization with access to third party and intenral intel services.
* Provided multiple **avenues of access** to support a variety of use cases: slack bot access, command line tools, python libraries.
* Leveraged **Einstein GPT** to provide contextually analysis of cumlative and disparate intelligence data to provide analysts inital assessments.
* (Almost) Entirely **serverless** infrastructure with IaC and the serverless framework.
* Usage started with hundreds of invokes per month, currently millions of invokes a month. 
* **Synapse Vertex** adoption. Created framework for developing, testing, and deploying in-house "Power-Ups". Re-architected system architecture for mirrored instances.
* Large emphasis on **mentoring** new-grad engineers and analysts with a desire to learn software.
* Lead and organized weekly pair programming sessions, multiple internal status meetings, and a weekly water cooler meeting. 
* Brought test coverage from 0% to 85% across dozens of repositories.

## Datadog | Threat Detection | October 2020 - March 2022

* Mentored junior team members and interns in coding standards, system design, clean coding practices, and test frameworks.
* Brought down the average amount lines of code for a detection by 40%.
* Lead effort for **continuous security testing** by running automated attacks.
* Overhauled custom alerting platform with deduplication, centralized response management and provide traceability, accountability, and metrics.
* Participated in hiring process across multiple teams, interviewing or reviewing projects of over 20 candidates.
* Maintained and improve legacy infrastructure and applications through security updates, deployments, bug fixes, and features.

## Secureworks | Counter Threat Unit | July 2015 - October 2020
**Senior Information Research Advisor**

* Developed and maintained a **malware analysis** repository supporting hundreds of threat 
analysts, analyzing tens of thousands of samples and generating dozens of terabytes 
of information a day. Fully supported with automated discovery, classifying, family
documentation, and a powerful full text search API.
* Delivered deployment pipelines to deliver zero downtime continuous delivery of 
python web applications through Gitlab **CI/CD using Terraform, AWS**. 
* To support a micro-services framework, developed a template project to allow others 
to deploy a container to AWS ECS with a single Make command using terraform, bash, 
and AWS. Complete with a springboard for unit tests.
* OSINT Vulnerability scraping to automate away hours of time for technical writers.
* Led effort to move tens of thousands of lines of python across multiple projects to 
**Python** 3 using test ratcheting, open source tools, and good old fashioned plug and chug.

## IID | Threat Intelligence | April 2012 - July 2015
**Threat Intelligence Analyst | Developer**

* Created python-based **algorithmic systems** in determining 'randomly' generated domains 
(DGA), and 'fast-fluxed' domains.
* Designed solutions to help the threat intelligence team better **research threats. 
GUIs for harvested spam templates, IP maps of infiltrated botnets, botnet emulators, 
DGA generators, etc**.
* Performed **dynamic analysis** of malware threats in sandboxes using memory dumps, 
registry key monitoring, network analysis, and command line 'scanners' for specific 
documents.
* Codeveloped "Cumulative Intelligence Search Tool" which leverages dozens of APIs
 (both internal and external) to quickly return high-level results to users for 
 malicious indicators (hashes, urls, hosts, ips, and uploaded samples). The tool was 
 designed to be internal, but has grew to have consistent users from over a dozen 
 organizations. Further, it became a leading product offering after being turned 
 over to a team of engineers.

## Education

**Western Washington University** | BS in Mathematics | Minor in Economics

--

This resume has a [CI/CD pipeline](https://gitlab.com/leumas/samchristian-info/-/blob/master/content/resume.md). Hosted at [https://www.samchristian.info/resume/](https://www.samchristian.info/resume/)

