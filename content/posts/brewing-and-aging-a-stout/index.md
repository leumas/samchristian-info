---
title: "Brewing and Aging an Imperial Stout."
description: "Lessons learned from brewing an Imperial Stout"
date: 2020-06-30T08:01:24-07:00
draft: false
tags: [
    "Homebrewing",
    "Beer",
    "Hobbies",
]
---

Brewing an imperial stout is a great step into intermediate home brewing. 
When compared to a typical recipe, an imperial stout has a few extra precautions 
and nuances that can heavily impact your finished product.

<!--more-->

## Nuances of Fermentation

1. Aerate your wort! Yeast need a large amount of oxygen to withstand the alcohol during
 fermentation. Using a diffusion stone is ideal - however, vigorous shaking of the 
 fermenter can still result in a great fermentation. Some mornings, during primary 
 fermentation, give the fermenter a shake to ensure the yeast is suspended.
1. Temperature control! Without proper temperature control the heat byproduct of the 
fermentation process could result in off flavors or overactive yeast. The probe should 
be put directly into the wort. I used our keezer that was emtpy to keep the fermentation 
down.
1. Blow off tube! Always use a blow off tube!
1. Yeast yeast yeast! You will definitely need a large starter for any snap pack type 
yeasts. For dry yeasts, use 3 packets and pitch directly into the wort 
(US-05 is a great yeast for stouts.)

A recent beer my wife and I brewed turned out around `1.105 OG`.
We were able to ferment it down to about `1.027 FG` for an ABV of about 10.25!

{{< figure src="img/gravity.jpg" alt="Original Gravity" width="640px">}}

## Second Runnings Stout

A beautiful thing about brewing big beers is the opportunity for a second beer.
By mashing the same, used, grain bed from the imperial stout you can end up with a 
nice 4% breakfast stout that can be enjoyed while your big beer ages.

## Aging

During aging, some flavor can be added by:

1. Adding adjuncts - fruits or spices.
1. Wood chips, wood blocks, or barrel aging

For the aforementioned beer, I decided to age the beer on bourbon soaked oak chips. My
wife decided on vanilla beans and figs. We had some cocoa nibs laying around
and those rounded out the flavor.

At the end of our brew day, we put 1 ounce of wood chips and a cup of bourbon into
 a quart sized, covered, mason jar. We shook the jar every few days.

{{< figure src="img/wood.jpg" alt="Wood Chips" width="640px">}}

After primary fermentation - about 14 days from brew day - we sliced a few old Vanilla
beans we had and racked the beer onto the beans and wood chips - as a homebrewer you
can add the bourbon in along with the the wood - professionals brewers are legally not
allowed to do this. 

Wood chips provide a large amount of surface area for the beer to pull flavor from. You
should try the beer after a week - and determine if the wood flavor is enough for you.
We took ours out after about 14 days - I wish we would have let it age a little longer.
If using wood bricks or a barrel this process will take much longer, but will benefit
the wood flavor - becoming more mellow and natural.

Finally, we racked the beer onto frozen figs that had been roughly chopped (quarters).
We let the beer age on the fruit for only 4 days and bottled. We used a very small
amount of priming sugar to under carbonate the beer. Unfortunately, the finished product
was over carbonated. Likely due to fermenting the extra sugars from the dates - but 
possibly because of infection.

The total time fermenting and aging was around 5 weeks.

From there, we have let the bottles age for about 4 months, and the beer continues
to mature its flavor.

## Presentation

After letting the bottles sit in a closet, we decided to wax the bottles! The process
took about an hour and gave the bottles a very defined look.

{{< figure src="img/waxed.jpg" alt="Bottles" width="640px">}}

