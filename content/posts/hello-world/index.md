---
title: "Hello World!"
description: "How does this site work?"
date: 2020-05-17T10:50:45-07:00
draft: false
tags: [
    "hugo",
    "ci/cd",
    "gitlab",
    "technical",
]
---

This website is maintained through gitlab's `pages` feature and delivered continuously
via gitlab-ci.

<!--more-->

## Resources

* [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
* [Hugo](https://gohugo.io/)
* [Gitlab pages tutorial](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#:~:text=%20Step-by-step%20%201%20Step%201%3A%20Create%20a,an%20eye%20on%20the%20build%21%20Don%27t...%20More%20)
* [Website Repository](https://gitlab.com/leumas/samchristian-info)
